describe books.catalog;

describe book;

SELECT
book.book_id,
book.title,
book.year_published,
book.num_pages,
book.in_print
FROM 
book;


SELECT
book.book_id,
book.title,
book.year_published,
book.num_pages,
book.in_print,
author.name as author,
author.country as author_country
FROM 
book,
author
WHERE
book.author_id = author.author_id;



SELECT
book.book_id,
book.title,
book.year_published,
book.num_pages,
book.in_print,
author.name as author,
author.country as author_country,
publisher.name as publisher,
publisher.city as publisher_city,
publisher.phone as publisher_phone
FROM 
book,
author,
publisher
WHERE
book.author_id = author.author_id
AND
book.publisher_id = publisher.publisher_id;



SELECT
book.book_id,
book.title,
book.year_published,
book.num_pages,
book.in_print,
author.name as author,
author.country as author_country,
publisher.name as publisher,
publisher.city as publisher_city,
publisher.phone as publisher_phone,
genre.name as genre
FROM 
book,
author,
publisher,
genre
WHERE
book.author_id = author.author_id
AND
book.publisher_id = publisher.publisher_id
AND
book.genre_id = genre.genre_id;



SELECT
book.book_id,
book.title,
book.year_published,
book.num_pages,
book.in_print,
author.name as author,
author.country as author_country,
publisher.name as publisher,
publisher.city as publisher_city,
publisher.phone as publisher_phone,
genre.name as genre,
format.name as format
FROM 
book,
author,
publisher,
genre,
format
WHERE
book.author_id = author.author_id
AND
book.publisher_id = publisher.publisher_id
AND
book.genre_id = genre.genre_id
AND
book.format_id = format.format_id;




ALTER TABLE book ADD modified_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP;


-- IMPLICIT JOIN
-- after WHERE must type link between tables
SELECT
book.book_id,
book.title,
book.year_published,
book.num_pages,
book.in_print,
author.name as author,
author.country as author_country,
publisher.name as publisher,
publisher.city as publisher_city,
publisher.phone as publisher_phone,
genre.name as genre,
book.price as price,
format.name as format,
book.modified_at
FROM 
book,
author,
publisher,
genre,
format
WHERE
book.author_id = author.author_id
AND
book.publisher_id = publisher.publisher_id
AND
book.genre_id = genre.genre_id
AND
book.format_id = format.format_id;



-- EXPLICIT JOIN with ON
SELECT
book.book_id,
book.title,
book.year_published,
book.num_pages,
book.in_print,
author.name as author,
author.country as author_country,
publisher.name as publisher,
publisher.city as publisher_city,
publisher.phone as publisher_phone,
genre.name as genre,
book.price as price,
format.name as format,
book.modified_at
FROM
book
JOIN author on book.author_id = author.author_id
JOIN publisher on book.publisher_id = publisher.publisher_id
JOIN format on book.format_id = format.format_id
JOIN genre on book.genre_id = genre.genre_id;


-- explicit join with USING (works when columns names in both table are the same)
SELECT
book.book_id,
book.title,
book.year_published,
book.num_pages,
book.in_print,
author.name as author,
author.country as author_country,
publisher.name as publisher,
publisher.city as publisher_city,
publisher.phone as publisher_phone,
genre.name as genre,
book.price as price,
format.name as format,
book.modified_at
FROM
book
JOIN author USING(author_id)
JOIN publisher USING(publisher_id)
JOIN genre USING(genre_id)
JOIN format USING(format_id);























