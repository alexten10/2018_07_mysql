select * from catalog where author="Stephen King";

select book_id, title, author, price from catalog where author="Stephen King";

select book_id, title, author, price from catalog where author="Stephen King" or author="Frank Herbert";

select book_id, title, author, price from catalog where price<10;

same queries
select book_id, title, author, price from catalog where price>=8 and price<=12;
select book_id, title, author, price from catalog where price between 8 and 12;

same result queries
select book_id, title, author, price from catalog where author_country != 'USA';
select book_id, title, author, price from catalog where author_country <> 'USA';
select book_id, title, author, price from catalog where author_country not in ('USA');

select book_id, title, author, price from catalog where author_country not in ('USA', 'Canada');

select book_id, title, author, price from catalog where author_country in ('USA', 'Canada');
select book_id, title, author, price from catalog where author_country='USA' or author_country='Canada';


select title, author, publisher, year_published from catalog where year_published between 2000 and 2006 and author_country = "Canada" and in_print=1;
select title, author, publisher, year_published from catalog where year_published between 2000 and 2006 and author_country = "Canada" and in_print !=0;











