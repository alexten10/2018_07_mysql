
-- add fulltext index
ALTER TABLE catalog
ADD FULLTEXT(title, author);





-- run full search
SELECT
title,
author
FROM
catalog
WHERE MATCH(title, author)
AGAINST('king carrie');

-- same as (IN NATURAL LANGUAGE MODE(default))

SELECT
title,
author
FROM
catalog
WHERE MATCH(title, author)
AGAINST('king carrie' IN NATURAL LANGUAGE MODE);



-- search for king but without carrie(-carrie)
-- sql cant search for - (hyphen) in boolean
SELECT
title,
author
FROM
catalog
WHERE MATCH(title, author)
AGAINST('king -carrie' IN BOOLEAN MODE);




-- natural mode on joined results
SELECT
book.title,
author.name
FROM
book
JOIN author using(author_id)
WHERE MATCH(book.title, author.name)
AGAINST('king carrie');
-- gives result ERROR 1210 (HY000): Incorrect arguments to MATCH




-- natural mode on joined results
SELECT
book.title,
author.name
FROM
book
JOIN author using(author_id)
WHERE MATCH(book.title)
AGAINST('king carrie')
OR
MATCH(author.name)
AGAINST('king carrie');





-- create view book_view
CREATE VIEW book_view
AS 
SELECT
book.title,
author.name as author,
publisher.name as publisher,
format.name as format,
genre.name as genre
FROM
book
JOIN author USING(author_id)
JOIN publisher USING(publisher_id)
JOIN format USING(format_id)
JOIN genre USING(genre_id);






SELECT
title,
author
FROM 
book_view
WHERE MATCH(title, author)
AGAINST('king carrie' IN NATURAL LANGUAGE MODE);
-- gives an error ERROR 1210 (HY000): Incorrect arguments to MATCH










































