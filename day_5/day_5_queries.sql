



-- shows all authors with duplicates
SELECT
author.name as author
FROM
book
JOIN author USING (author_id);


-- show all authors WITHOUT duplicates
SELECT
DISTINCT author.name as author
FROM
book
JOIN author USING (author_id);






SELECT
book.title,
book.num_pages,
book.price,
author.name as author,
publisher.name as publisher
FROM 
book
JOIN author USING (author_id)
JOIN publisher USING (publisher_id);


-- limiting number of results
SELECT
book.title,
book.num_pages,
book.price,
author.name AS author,
publisher.name AS publisher
FROM 
book
JOIN author USING (author_id)
JOIN publisher USING (publisher_id)
LIMIT 5;



-- limiting number of results starting showing after 10 results (first 10 lines are not shown)
SELECT
book.title,
book.num_pages,
book.price,
author.name AS author,
publisher.name AS publisher
FROM 
book
JOIN author USING (author_id)
JOIN publisher USING (publisher_id)
LIMIT 10,5;



alias 'mysql'='winpty mysql'
alias 'sqlite3'='winpty sqlite3'



-- replace value
REPLACE INTO
book
(book_id, title)
VALUES
(3, 'A new Book');



-- clears the table
truncate book;


INSERT INTO book
(book_id, title, price)
VALUES
(12, 'Caves of steel', 12.99)
ON DUPLICATE KEY UPDATE
price = 13.99;






























