
-- Schema (meets 1NF and 2NF):
employee ([emp_id], name, email, phone, age, dept, dept_manager, start_date)




-- now make meet 3NF:
-- dept determines dept_manager

employee ([emp_id], name, email, phone, age, dep_id, start_date)
dept([dep_id], dept, dept_manager)





-- but if phone belongs to a department then schema will be
-- dept determines dept_manager and phone

employee ([emp_id], name, email, age, dep_id, start_date)
dept([dep_id], dept, dept_manager, phone)

