
-- to see structure of each needed table
describe book;
describe publisher;
describe author;
describe genre;



-- select title, author and publisher
SELECT
book.title,
author.name AS author,
publisher.name AS publisher
FROM
book,
publisher,
author
WHERE
book.publisher_id = publisher.publisher_id
AND
book.author_id = author.author_id;

-- select title, author and publisher
SELECT
book.title,
author.name AS author,
publisher.name AS publisher
FROM
book
JOIN author using (author_id)
JOIN publisher using (publisher_id);


-- title, autor, publisher, genre, price author ends in king
SELECT
book.title,
author.name as author,
publisher.name as publisher,
genre.name as genre,
book.price
FROM book
JOIN author using(author_id)
JOIN publisher using(publisher_id)
JOIN genre using (genre_id)
WHERE
author.name
LIKE
"%king";

-- title, author_name, author_country if the author is from USA or Canada
SELECT
book.title,
author.name as author_name,
author.country as author_country
FROM
book
JOIN author using(author_id)
WHERE
author.country = "USA"
OR 
author.country = "Canada"
ORDER BY
author.country
ASC;

-- title, author_name, author_country if the author is NOT from USA or Canada
SELECT
book.title,
author.name as author_name,
author.country as author_country
FROM
book
JOIN author using(author_id)
WHERE
author.country != "USA"
AND 
author.country != "Canada"
ORDER BY
author.country
ASC;



-- title, author, publisher, price where books are cheaper than $18
SELECT
book.title,
author.name as author,
publisher.name as publisher,
book.price
FROM
book
JOIN author using(author_id)
JOIN publisher using (publisher_id)
WHERE
book.price < 18;






-- title, author, publisher, price where price between 10 and 25
SELECT
book.title,
author.name as author,
publisher.name as publisher,
book.price
FROM
book
JOIN author using(author_id)
JOIN publisher using (publisher_id)
WHERE
book.price
BETWEEN
10
AND
25;




-- add publisher
describe publisher;

INSERT INTO
publisher
(
name,
city,
phone
)
VALUES
(
"S_publisher",
"Moscow",
704-999-6699
);




-- add author
describe author;

INSERT INTO
author
(
name,
country
)
VALUES
(
"Kot v sapogah",
"Russia"
);





--add book without publisher_id
describe book;

INSERT INTO 
book
(
title,
year_published,
num_pages,
in_print,
price,
author_id,
format_id,
genre_id
)
VALUES
(
"Zolotoy kluch",
1800,
29,
1,
1.99,
99,
199,
299
);


SELECT * FROM book;


SELECT
book.book_id,
book.title,
book.year_published,
book.num_pages,
book.in_print,
author.name as author,
author.country as author_country,
publisher.name as publisher,
publisher.city as publisher_city,
publisher.phone as publisher_phone,
genre.name as genre,
book.price as price,
format.name as format,
book.modified_at
FROM
book
JOIN author USING(author_id)
JOIN publisher USING(publisher_id)
JOIN genre USING(genre_id)
JOIN format USING(format_id)
ORDER BY 
book_id;







-- books where we didnt add an author
SELECT
book.book_id,
book.title,
author.author_id,
author.name as author
FROM
book
LEFT JOIN author using (author_id);


SELECT
book.book_id,
book.title,
author.author_id,
author.name as author
FROM
book
RIGHT JOIN author using (author_id);


SELECT
book.book_id,
book.title,
author.author_id,
author.name as author
FROM
book
RIGHT JOIN author using (author_id);
WHERE
book.book_id IS NULL;




-- find books where no genre has been assigned
SELECT 
book.book_id,
book.title,
genre.genre_id,
genre.name as genre
FROM
book
LEFT JOIN genre using (genre_id);


SELECT 
book.book_id,
book.title,
genre.genre_id,
genre.name as genre
FROM
book
LEFT JOIN genre using (genre_id)
WHERE 
genre.genre_id is NULL;



SELECT
book.book_id,
book.title,
genre.genre_id,
genre.name as genre
FROM 
book
CROSS JOIN genre;
-- same as
SELECT
book_id,
book.title,
genre.genre_id,
genre.name as genre
FROM 
book,
genre;


















