




select price * 1.1 from book;

select price, price * 1.1 as "new price" from book;

-- increasing prise by 10%
UPDATE
book
SET
price = price * 1.1;



SELECT
book_id,
title,
price AS cost,
FORMAT (price * 1.5, 2) AS cust_price
FROM 
book;



SELECT
book.book_id,
book.title,
book.year_published,
book.num_pages,
book.in_print,
author.name as author,
author.country as author_country,
publisher.name as publisher,
publisher.city as publisher_city,
publisher.phone as publisher_phone,
genre.name as genre,
book.price as price,
format.name as format,
book.modified_at
FROM
book
JOIN author USING(author_id)
JOIN publisher USING(publisher_id)
JOIN genre USING(genre_id)
JOIN format USING(format_id);

--- same as 

SELECT
b.book_id,
b.title,
b.year_published,
b.num_pages,
b.in_print,
a.name as author,
a.country as author_country,
p.name as publisher,
p.city as publisher_city,
p.phone as publisher_phone,
g.name as genre,
b.price as price,
f.name as format,
b.modified_at
FROM
book b
JOIN author a USING(author_id)
JOIN publisher p USING(publisher_id)
JOIN genre g USING(genre_id)
JOIN format f USING(format_id);

--same as
SELECT
b.book_id,
b.title,
b.year_published,
b.num_pages,
b.in_print,
a.name as author,
a.country as author_country,
p.name as publisher,
p.city as publisher_city,
p.phone as publisher_phone,
g.name as genre,
b.price as price,
f.name as format,
b.modified_at
FROM
book b
JOIN author AS a USING(author_id)
JOIN publisher AS p USING(publisher_id)
JOIN genre AS g USING(genre_id)
JOIN format AS f USING(format_id);


--same as
SELECT
b.book_id,
b.title,
b.year_published,
b.num_pages,
b.in_print,
a.name as author,
a.country as author_country,
p.name as publisher,
p.city as publisher_city,
p.phone as publisher_phone,
g.name as genre,
b.price as price,
f.name as format,
b.modified_at
FROM
book AS b, author AS a, publisher p, genre g, format f
WHERE
b.author_id = a.author_id
AND
b.publisher_id = p.publisher_id
AND
b.genre_id = g.genre_id
AND
b.format_id = f.format_id;


-- shows number of row in results set
SELECT COUNT(*) FROM book;

SELECT COUNT(*) FROM author WHERE name LIKE "%king";

-- this will show 14.290000
SELECT AVG(price) as "average price" FROM book;

-- this will show 14.29
SELECT FORMAT (AVG(price),2) as "average price" FROM book;

SELECT FORMAT (MAX(price),2) as "maximum price" FROM book;

SELECT FORMAT (MIN(price),2) as "minimum price" FROM book;


SELECT
MIN(price) as 'Minimum price',
MAX(price) as 'MAximum price',
AVG(price) as 'Average price'
FROM
book;

-- show number of books per publisher
SELECT
publisher.name,
count(book.book_id) as 'num_book'
FROM 
book
JOIN publisher USING (publisher_id)
group by publisher.name;


-- show number of books by publisher and author(within publisher)
SELECT
publisher.name as publisher,
author.name as author,
count(book.book_id) as 'num_book'
FROM 
book
JOIN publisher USING (publisher_id)
JOIN author USING (author_id)
group by publisher.name, author.name;


-- show min max and avg prices by publisher
SELECT
publisher.name as publisher_name,
MIN(price) as 'Min_price',
MAX(price) as 'MAx_price',
AVG(price) as 'Avg_price'
FROM
book
JOIN publisher using (publisher_id)
GROUP BY publisher.name;



-- show number of books per genre
SELECT
genre.name as genre_name,
count(book.book_id) as num_book
FROM
book
JOIN genre using (genre_id)
GROUP BY genre.name;



-- number of books per publisher only if number of books >2
SELECT
publisher.name as publisher_name,
COUNT(book.book_id) as num_book
FROM
book
JOIN publisher using (publisher_id)
GROUP BY publisher.name
HAVING num_book>2;


-- number of books by publisher and per format within publisher
SELECT
publisher.name AS publisher_name,
format.name as format_name,
COUNT(book.book_id) as number_of_books
FROM
book
JOIN publisher using (publisher_id)
JOIN format USING (format_id)
GROUP BY publisher.name, format.name;




SELECT
book.book_id,
book.title,
book.price,
author.name as author,
publisher.name as publisher,
format.name as format,
genre.name as genre
FROM 
book
JOIN author USING(author_id)
JOIN publisher USING(publisher_id)
JOIN format USING(format_id)
JOIN genre USING(genre_id);



-- 1
--- creates a table with the SELECT result
CREATE VIEW book_list
AS
SELECT
book.book_id,
book.title,
book.price,
author.name as author,
publisher.name as publisher,
format.name as format,
genre.name as genre
FROM 
book
JOIN author USING(author_id)
JOIN publisher USING(publisher_id)
JOIN format USING(format_id)
JOIN genre USING(genre_id)
ORDER BY book.book_id ASC;

-- 2
-- change title for book_id 2
-- it also changes table book too!!!
UPDATE book_list
SET title = 'Sunny Island'
WHERE
book_id = 2;

-- 3
-- if want delete the created view
DROP VIEW book_list;










