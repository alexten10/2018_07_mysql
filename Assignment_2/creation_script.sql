

DROP DATABASE IF EXISTS capstone;

CREATE DATABASE capstone;

USE capstone;

CREATE TABLE product
(
product_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
sku CHAR(8),
name VARCHAR(255),
price FLOAT(6,2),
in_stock BOOL,
description VARCHAR(4000),
weight FLOAT(4,2),
best_before VARCHAR(255),
rating INT(1),
quantity INT(5),
category_id INT(11),
manufacturer_id INT(11),
discount_id INT(11),
created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
updated_at DATETIME DEFAULT NULL,
supplier_id INT(11)
);


INSERT INTO product
(sku, name, price, in_stock, description, weight, best_before, rating, quantity, category_id, manufacturer_id, discount_id, supplier_id)
VALUES
("c0000001", "Tim Horton's Single Serve 12count", 31.69, 1, "Original Blend, 12 count", 0.3, "2018-09-07", 5, 12, 1, 5, 1, 15),
("c0000002", "Tim Horton's Single Serve 24 count", 62.99, 1, "Original Blend, 24 count", 0.6, "2018-10-07", 5, 24, 1, 5, 1, 15),
("c0000003", "Tim Horton's Ground Coffee", 19.99, 0, "Medium-bodied coffee with smooth finish", 0.91, "2018-08-04", 4, 1, 2, 5, 2, 3),
("c0000004", "Tim Horton's Whole Bean Coffee 12oz", 17.99, 1, "Medium-bodied coffee with smooth finish", 0.34, "2018-12-06", 4, 1, 2, 5, 3, 3),
("c0000005", "Tim Horton's Whole Bean Coffee 2pound", 45.99, 0, "Medium-bodied coffee with smooth finish", 0.91, "2019-01-06", 3, 1, 2, 5, 1, 3),
("c0000006", "Tim Horton's Original Roast 12oz", 23.89, 1, "A perfectly balanced, medium-bodied coffee", 0.34, "2018-10-07", 5, 1, 1, 5, 1, 15),
("c0000007", "Tim Horton's Dark Roast 12oz", 23.89, 1, "Rich, smooth, full flavor of Tim Hortons", 0.34, "2018-11-19", 3, 1, 3, 5, 3, 2),
("c0000008", "Tim Horton's Decaffeinated 12count", 31.69, 0, "Decaffeinated by the Swiss Water Process", 0.3, "2019-02-03", 2, 12, 1, 5, 2, 2),
("c0000009", "Tim Horton's Decaffeinated 24count", 62.99, 1, "Decaffeinated by the Swiss Water Process", 0.6, "2020-01-01", 2, 24, 1, 5, 2, 2),
("c0000010", "Tim Horton's Decaffeinated Ground 12oz", 23.89, 1, "Decaffeinated by the Swiss Water Process", 0.34, "2019-04-22", 4, 1, 8, 4, 1, 6),
("c0000011", "Tim Horton's French Coffee 12count", 31.69, 1, "Coffee with French Vanilla sweetness", 0.3, "2018-11-17", 5, 12, 6, 5, 2, 1),
("c0000012", "Tim Horton's French Coffee 24count", 62.99, 1, "Coffee with French Vanilla sweetness", 0.6, "2018-11-22", 5, 24, 6, 5, 3, 1),
("c0000013", "Tim Horton's French Vanila Ground 12oz", 23.89, 1, "Coffee with French Vanilla sweetness", 0.34, "2021-04-08", 5, 1, 4, 5, 1, 7),
("c0000014", "Tim Horton's French Vanila Ground 24oz",62.99, 1, "Coffee with French Vanilla sweetness", 0.68, "2020-07-07", 4, 1, 1, 5, 1, 15),
("c0000015", "Tim Horton's Instant Cappuccino 16oz",38.99, 1, "Creamy French vanilla flavor", 0.45, "2018-09-07", 5, 12, 1, 5, 1, 15),
("t0000001", "Creamy Oolong Tea", 19.99, 1, "Creamy flavour almost vanilla like", 0.1, "2025-05-07", 5, 1, 11, 9, 1, 4),
("t0000002", "Gaba Oolong Tea", 9.99, 1, "Wonderful, soothing flavour", 0.1, "2022-10-24", 4, 1, 11, 9, 3, 4),
("t0000003", "Assam Bukhial Tea", 6.99, 1, "Orthodox leaf style", 0.1, "2020-06-09", 3, 1, 12, 9, 1, 8),
("t0000004", "Admiral Earl Grey Tea", 15.99, 1, "A blend of Darjeeling and oil of bergamot", 0.1, "2023-12-07", 5, 1, 12, 15, 2, 8),
("t0000005", "Imperial Matcha Tea", 29.99, 1, "Shade-grown tea", 0.1, "2022-02-27", 4, 1, 12, 6, 3, 7);



















