-- MySQL dump 10.16  Distrib 10.2.12-MariaDB, for Win64 (AMD64)
--
-- Host: localhost    Database: capstone
-- ------------------------------------------------------
-- Server version	10.2.12-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `sku` char(8) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `price` float(6,2) DEFAULT NULL,
  `in_stock` tinyint(1) DEFAULT NULL,
  `description` varchar(4000) DEFAULT NULL,
  `weight` float(4,2) DEFAULT NULL,
  `best_before` varchar(255) DEFAULT NULL,
  `rating` int(1) DEFAULT NULL,
  `quantity` int(5) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `manufacturer_id` int(11) DEFAULT NULL,
  `discount_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (1,'c0000001','Tim Horton\'s Single Serve 12count',31.69,1,'Original Blend, 12 count',0.30,'2018-09-07',5,12,1,5,1,'2018-07-22 03:15:48',NULL,15),(2,'c0000002','Tim Horton\'s Single Serve 24 count',62.99,1,'Original Blend, 24 count',0.60,'2018-10-07',5,24,1,5,1,'2018-07-22 03:15:48',NULL,15),(3,'c0000003','Tim Horton\'s Ground Coffee',19.99,0,'Medium-bodied coffee with smooth finish',0.91,'2018-08-04',4,1,2,5,2,'2018-07-22 03:15:48',NULL,3),(4,'c0000004','Tim Horton\'s Whole Bean Coffee 12oz',17.99,1,'Medium-bodied coffee with smooth finish',0.34,'2018-12-06',4,1,2,5,3,'2018-07-22 03:15:48',NULL,3),(5,'c0000005','Tim Horton\'s Whole Bean Coffee 2pound',45.99,0,'Medium-bodied coffee with smooth finish',0.91,'2019-01-06',3,1,2,5,1,'2018-07-22 03:15:48',NULL,3),(6,'c0000006','Tim Horton\'s Original Roast 12oz',23.89,1,'A perfectly balanced, medium-bodied coffee',0.34,'2018-10-07',5,1,1,5,1,'2018-07-22 03:15:48',NULL,15),(7,'c0000007','Tim Horton\'s Dark Roast 12oz',23.89,1,'Rich, smooth, full flavor of Tim Hortons',0.34,'2018-11-19',3,1,3,5,3,'2018-07-22 03:15:48',NULL,2),(8,'c0000008','Tim Horton\'s Decaffeinated 12count',31.69,0,'Decaffeinated by the Swiss Water Process',0.30,'2019-02-03',2,12,1,5,2,'2018-07-22 03:15:48',NULL,2),(9,'c0000009','Tim Horton\'s Decaffeinated 24count',62.99,1,'Decaffeinated by the Swiss Water Process',0.60,'2020-01-01',2,24,1,5,2,'2018-07-22 03:15:48',NULL,2),(10,'c0000010','Tim Horton\'s Decaffeinated Ground 12oz',23.89,1,'Decaffeinated by the Swiss Water Process',0.34,'2019-04-22',4,1,8,4,1,'2018-07-22 03:15:48',NULL,6),(11,'c0000011','Tim Horton\'s French Coffee 12count',31.69,1,'Coffee with French Vanilla sweetness',0.30,'2018-11-17',5,12,6,5,2,'2018-07-22 03:15:48',NULL,1),(12,'c0000012','Tim Horton\'s French Coffee 24count',62.99,1,'Coffee with French Vanilla sweetness',0.60,'2018-11-22',5,24,6,5,3,'2018-07-22 03:15:48',NULL,1),(13,'c0000013','Tim Horton\'s French Vanila Ground 12oz',23.89,1,'Coffee with French Vanilla sweetness',0.34,'2021-04-08',5,1,4,5,1,'2018-07-22 03:15:48',NULL,7),(14,'c0000014','Tim Horton\'s French Vanila Ground 24oz',62.99,1,'Coffee with French Vanilla sweetness',0.68,'2020-07-07',4,1,1,5,1,'2018-07-22 03:15:48',NULL,15),(15,'c0000015','Tim Horton\'s Instant Cappuccino 16oz',38.99,1,'Creamy French vanilla flavor',0.45,'2018-09-07',5,12,1,5,1,'2018-07-22 03:15:48',NULL,15),(16,'t0000001','Creamy Oolong Tea',19.99,1,'Creamy flavour almost vanilla like',0.10,'2025-05-07',5,1,11,9,1,'2018-07-22 03:15:48',NULL,4),(17,'t0000002','Gaba Oolong Tea',9.99,1,'Wonderful, soothing flavour',0.10,'2022-10-24',4,1,11,9,3,'2018-07-22 03:15:48',NULL,4),(18,'t0000003','Assam Bukhial Tea',6.99,1,'Orthodox leaf style',0.10,'2020-06-09',3,1,12,9,1,'2018-07-22 03:15:48',NULL,8),(19,'t0000004','Admiral Earl Grey Tea',15.99,1,'A blend of Darjeeling and oil of bergamot',0.10,'2023-12-07',5,1,12,15,2,'2018-07-22 03:15:48',NULL,8),(20,'t0000005','Imperial Matcha Tea',29.99,1,'Shade-grown tea',0.10,'2022-02-27',4,1,12,6,3,'2018-07-22 03:15:48',NULL,7);
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-07-22  3:20:40
